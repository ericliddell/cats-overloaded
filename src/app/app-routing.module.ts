import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from './core/services/auth-guard.service';
import { EmptyWrapperComponent } from './empty-wrapper/empty-wrapper.component';
import { SideMenuWrapperComponent } from './side-menu-wrapper/side-menu-wrapper.component';
import { HomeComponent } from './home/home.component';
import { BreedDetailsComponent } from './breed-details/breed-details.component';

const routes: Routes = [
  {
    path: 'login',
    component: EmptyWrapperComponent,
    children: [
      {
        path: '',
        component: AuthComponent,
      },
    ],
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    component: SideMenuWrapperComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: ':id',
        component: BreedDetailsComponent,
      },
    ],
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

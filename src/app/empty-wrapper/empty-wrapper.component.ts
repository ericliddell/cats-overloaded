import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empty-wrapper',
  templateUrl: './empty-wrapper.component.html',
  styleUrls: ['./empty-wrapper.component.scss']
})
export class EmptyWrapperComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

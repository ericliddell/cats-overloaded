import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { Breed } from '../models/breed.model';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class CatService {
  constructor(private apiService: ApiService) {}

  getBreeds(): Observable<Breed[]> {
    return this.apiService.get(`/breeds`).pipe(map((data) => data));
  }

  getBreedDetails(breedId: string): Observable<any> {
    return this.apiService
      .get(`/images/search?breed_id=${breedId}`)
      .pipe(map((data) => data[0]));
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class FakeAuthService {
  user: User = {
    token: 'superValidUserToken',
    username: 'admin',
  };

  getUser(): User {
    return this.user;
  }

  authenticate(credentials: any): User | undefined {
    if (
      credentials.username === 'admin' &&
      credentials.password === 'password'
    ) {
      return this.user;
    }
    return undefined;
  }
}

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { TokenService } from './token.service';
import { UserService } from './user.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private tokenService: TokenService, private routes: Router) {}

  canActivate(): boolean {
    if (this.tokenService.getToken()) {
      return true;
    } else {
      this.routes.navigate(['/login']);
      return false;
    }
  }
}

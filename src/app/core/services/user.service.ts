import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';

import { TokenService } from './token.service';
import { User } from '../models/user.model';
import { distinctUntilChanged } from 'rxjs/operators';
import { FakeAuthService } from './fake-auth.service';

@Injectable()
export class UserService {
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject
    .asObservable()
    .pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor(
    private fakeAuthApiService: FakeAuthService,
    private tokenService: TokenService
  ) {}

  populate() {
    if (this.tokenService.getToken()) {
      const user = this.fakeAuthApiService.getUser();
      if (user) {
        this.setAuth(user);
      }
    }
  }

  setAuth(user: User) {
    this.tokenService.saveToken(user.token);
    this.currentUserSubject.next(user);
    this.isAuthenticatedSubject.next(true);
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }

  attemptAuth(credentials: any): User | undefined {
    const user = this.fakeAuthApiService.authenticate(credentials);
    if (user) {
      this.setAuth(user);
    }
    return user;
  }
}

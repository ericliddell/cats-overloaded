export interface Breed {
  id: string;
  name: string;
  description: string;
  child_friendly: number;
  vcahospitals_url: string;
  wikipedia_url: string;
  url: string;
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './services/auth-guard.service';
import { ApiService } from './services/api.service';
import { UserService } from './services/user.service';
import { TokenService } from './services/token.service';
import { CatService } from './services/cat.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [
    ApiService,
    CatService,
    AuthGuard,
    UserService,
    TokenService
  ]
})
export class CoreModule {}

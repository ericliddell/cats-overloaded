import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Breed } from '../core/models/breed.model';
import { CatService } from '../core/services/cat.service';

@Component({
  selector: 'app-breed-details',
  templateUrl: './breed-details.component.html',
  styleUrls: ['./breed-details.component.scss'],
})
export class BreedDetailsComponent implements OnInit, OnDestroy {
  private routeSub: Subscription = new Subscription();
  isFetching: boolean = false;
  breedDetails: Breed | undefined;

  constructor(private route: ActivatedRoute, private catService: CatService) {}

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe((params) => {
      this.getBreedDetails(params['id']);
    });
  }

  getBreedDetails(breedId: string) {
    this.isFetching = true;
    this.catService.getBreedDetails(breedId).subscribe((res) => {
      if (res) {
        this.breedDetails = res.breeds[0];

        if (this.breedDetails) {
          this.breedDetails.url = res.url;
          this.isFetching = false;
        }
      }
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}

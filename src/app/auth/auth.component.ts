import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../core/services/user.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent {
  isSubmitting = false;
  authForm: FormGroup;
  error: string = '';

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {
    this.authForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  submitForm() {
    this.isSubmitting = true;
    this.error = '';

    const credentials = this.authForm.value;
    const user = this.userService.attemptAuth(credentials);
    if (user) {
      this.router.navigateByUrl('/home');
    } else {
      this.error = 'The username and password were not recognised';
      this.isSubmitting = false;
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideMenuWrapperComponent } from './side-menu-wrapper.component';

describe('SideMenuWrapperComponent', () => {
  let component: SideMenuWrapperComponent;
  let fixture: ComponentFixture<SideMenuWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SideMenuWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SideMenuWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

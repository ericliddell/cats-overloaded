import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { Breed } from '../core/models/breed.model';
import { CatService } from '../core/services/cat.service';

@Component({
  selector: 'app-side-menu-wrapper',
  templateUrl: './side-menu-wrapper.component.html',
  styleUrls: ['./side-menu-wrapper.component.scss'],
})
export class SideMenuWrapperComponent implements OnInit {
  @ViewChild('sidenav')
  sidenav!: MatSidenav;

  isExpanded = true;
  showSubmenu: boolean = true;
  isShowing = false;
  showSubSubMenu: boolean = false;

  breeds: Breed[] = [];

  constructor(private catService: CatService, private router: Router) {}

  ngOnInit(): void {
    this.catService.getBreeds().subscribe((res) => {
      this.breeds = res;
    });
  }

  getBreedInfo(breed: Breed) {
    this.router.navigate(['/home', breed.id]);
  }

  mouseenter() {
    if (!this.isExpanded) {
      this.isShowing = true;
    }
  }

  mouseleave() {
    if (!this.isExpanded) {
      this.isShowing = false;
    }
  }
}
